exports.seed = function (knex, Promise) {
  return knex('mealPlanInfo').del()
  .then(function () {
    return knex.raw('ALTER TABLE mealPlanInfo AUTO_INCREMENT = 1')
  })
  .then(function () {
    return knex('meal').del()
  })
  .then(function () {
    return knex.raw('ALTER TABLE meal AUTO_INCREMENT = 1')
  })
  .then(function () {
    return knex('mealPlanInfo').insert([
      {
        id: 1,
        name: '1200 Calorie Traditional',
        description: '1200 Calorie Traditional'
      },
      {
        id: 2,
        name: '2000 Calorie Traditional',
        description: '2000 Calorie Traditional'
      },
      {
        id: 3,
        name: '1500 Calorie Vegetarian',
        description: '1500 Calorie Vegetarian'
      }
    ])
  })
}

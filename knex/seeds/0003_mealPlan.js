exports.seed = function (knex, Promise) {
  return knex('mealPlan').del()
  .then(function () {
    return knex.raw('ALTER TABLE mealPlan AUTO_INCREMENT = 1')
  })
  .then(function () {
    return knex('mealPlan').insert([
      {
        id: 1,
        name: '1200 Calorie Meal Plan (Full Week - 21 Meals)',
        mealPlanInfoId: 1,
        mealCount: 21,
        cost: 136,
        pickupDay: 1,
        outOfAreaDeliveryDay: 2,
        active: 1,
        dayMask: 127
      },
      {
        id: 2,
        name: '1200 Calorie Meal Plan (First Part of Week - 12 Meals)',
        mealPlanInfoId: 1,
        mealCount: 12,
        cost: 96,
        pickupDay: 1,
        outOfAreaDeliveryDay: 2,
        active: 1,
        dayMask: 15
      },
      {
        id: 3,
        name: '1200 Calorie Meal Plan (Second Part of Week - 9 Meals)',
        mealPlanInfoId: 1,
        mealCount: 9,
        cost: 78,
        pickupDay: 4,
        outOfAreaDeliveryDay: 5,
        active: 1,
        dayMask: 112
      },
      {
        id: 4,
        name: '2000 Calorie Meal Plan (Full Week - 21 Meals)',
        mealPlanInfoId: 2,
        mealCount: 21,
        cost: 159,
        pickupDay: 1,
        outOfAreaDeliveryDay: 2,
        active: 1,
        dayMask: 127
      },
      {
        id: 5,
        name: '2000 Calorie Meal Plan (First Part of Week - 12 Meals)',
        mealPlanInfoId: 2,
        mealCount: 12,
        cost: 112,
        pickupDay: 1,
        outOfAreaDeliveryDay: 2,
        active: 1,
        dayMask: 15
      },
      {
        id: 6,
        name: '2000 Calorie Meal Plan (Second Part of Week - 9 Meals)',
        mealPlanInfoId: 2,
        mealCount: 9,
        cost: 92,
        pickupDay: 4,
        outOfAreaDeliveryDay: 5,
        active: 1,
        dayMask: 112
      },
      {
        id: 7,
        name: '1500 Calorie Vegetarian Meal Plan (Full Week - 21 Meals)',
        mealPlanInfoId: 3,
        mealCount: 21,
        cost: 136,
        pickupDay: 1,
        outOfAreaDeliveryDay: 2,
        active: 1,
        dayMask: 127
      },
      {
        id: 8,
        name: '1500 Calorie Vegetarian Meal Plan (First Part of Week - 12 Meals)',
        mealPlanInfoId: 3,
        mealCount: 12,
        cost: 96,
        pickupDay: 1,
        outOfAreaDeliveryDay: 2,
        active: 1,
        dayMask: 15
      },
      {
        id: 9,
        name: '1500 Calorie Vegetarian Meal Plan (Second Part of Week - 9 Meals)',
        mealPlanInfoId: 3,
        mealCount: 9,
        cost: 78,
        pickupDay: 4,
        outOfAreaDeliveryDay: 5,
        active: 1,
        dayMask: 112
      }
    ])
  })
}

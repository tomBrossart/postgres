exports.seed = function (knex, Promise) {
  return knex('user').del()
  .then(function () {
    return knex.raw('ALTER TABLE user AUTO_INCREMENT = 1')
  })
  .then(function () {
    return knex('user').insert([
      {
        id: 1,
        email: 'joel@codeinversion.com',
        password: '$2a$10$bpLpaKY.2sJVB4sLHtdU/.iCMuHxBg80ipIcWAj7lqqApaQtP99eS',
        roles: '["user", "admin", "superAdmin"]',
        firstName: 'Joel',
        lastName: 'Stewart'
      },
      {
        id: 2,
        email: 'jfokes@codeinversion.com',
        password: '$2a$10$bpLpaKY.2sJVB4sLHtdU/.iCMuHxBg80ipIcWAj7lqqApaQtP99eS',
        roles: '["user", "admin", "superAdmin"]',
        firstName: 'Justin',
        lastName: 'Fokes'
      },
      {
        id: 3,
        email: 'psaxton@codeinversion.com',
        password: '$2a$10$bpLpaKY.2sJVB4sLHtdU/.iCMuHxBg80ipIcWAj7lqqApaQtP99eS',
        roles: '["user", "admin", "superAdmin"]',
        firstName: 'Patrick',
        lastName: 'Saxton'
      },
      {
        id: 4,
        email: 'stef@healthyforlifemeals.com',
        password: '$2a$10$bpLpaKY.2sJVB4sLHtdU/.iCMuHxBg80ipIcWAj7lqqApaQtP99eS',
        roles: '["user", "admin", "superAdmin"]',
        firstName: 'Stef',
        lastName: 'Keegan'
      },
      {
        id: 5,
        email: 'christie@qonqr.com',
        password: '$2a$10$bpLpaKY.2sJVB4sLHtdU/.iCMuHxBg80ipIcWAj7lqqApaQtP99eS',
        roles: '["user", "admin", "superAdmin"]',
        firstName: 'Christie',
        lastName: 'Judd'
      },
      {
        id: 6,
        email: '2@dist.com',
        password: '$2a$10$bpLpaKY.2sJVB4sLHtdU/.iCMuHxBg80ipIcWAj7lqqApaQtP99eS',
        roles: '["user", "admin"]',
        firstName: 'Distributor',
        lastName: 'Two',
        customClaims: '{"distributors": [2]}'
      },
      {
        id: 7,
        email: '3@dist.com',
        password: '$2a$10$bpLpaKY.2sJVB4sLHtdU/.iCMuHxBg80ipIcWAj7lqqApaQtP99eS',
        roles: '["user", "admin"]',
        firstName: 'Distributor',
        lastName: 'Three',
        customClaims: '{"distributors": [3,4]}'
      },
      {
        id: 8,
        email: '4@dist.com',
        password: '$2a$10$bpLpaKY.2sJVB4sLHtdU/.iCMuHxBg80ipIcWAj7lqqApaQtP99eS',
        roles: '["user", "admin"]',
        firstName: 'Distributor',
        lastName: 'Four',
        customClaims: '{"distributors": [5,6,7]}'
      }
    ])
    .then(_ =>{
      return knex('address').del()
    })
    .then(function () {
      return knex.raw('ALTER TABLE address AUTO_INCREMENT = 1')
    })
    .then(_ => {
      return knex('address').insert([
        {
          id: 1,
          userId: 3,
          address1: '4665 Heather Ridge Rd N',
          city: 'Oakdale',
          state: 'MN',
          zip: '55128',
          'default': 1,
          'billing': 1
        },
        {
          id: 2,
          userId: 3,
          address1: '2586 7th Ave E',
          address2: 'Suite 303',
          city: 'North Saint Paul',
          state: 'MN',
          zip: '55109',
          'default': 0,
          'billing': 0
        }
      ])
    })
    .then(_ =>{
      return knex('session').del()
    })
  })
}

exports.up = function (knex, Promise) {
	return Promise.all([
		knex.schema.createTable('session', (table) => {
			table.charset('latin1');
			table.string('token', 766).notNullable().primary();
			table.text('tokenKey').notNullable();
		})
	]);
};

exports.down = function (knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('session')
	]);
};

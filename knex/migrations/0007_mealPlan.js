exports.up = function (knex, Promise) {
	return Promise.all([
		knex.schema.createTable('mealPlan', (table) => {
			table.increments('id').primary();
			table.string('name', 255).notNullable().defaultTo('');
			table.integer('mealPlanInfoId').unsigned().references('mealPlanInfo.id').onUpdate('CASCADE').onDelete('SET NULL');
			table.integer('mealCount').unsigned().notNullable().defaultTo(0);
			table.float('cost').notNullable().defaultTo(0);
			table.integer('pickupDay').unsigned().notNullable().defaultTo(0);
			table.integer('outOfAreaDeliveryDay').unsigned().notNullable().defaultTo(0);
			table.boolean('active').notNullable().defaultTo(true);
			table.integer('dayMask', 1).notNullable().defaultTo(0);
		})
	]);
};

exports.down = function (knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('mealPlan')
	]);
};
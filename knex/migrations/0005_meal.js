exports.up = function (knex, Promise) {
	return Promise.all([
		knex.schema.createTable('meal', (table) => {
			table.increments('id').primary();
			table.string('npName', 255).unique().notNullable().defaultTo('');
			table.string('name', 255).notNullable().defaultTo('');
			table.string('imageUrl', 255).nullable();
			table.text('description').notNullable().defaultTo('');
			table.integer('mealTime').unsigned().notNullable().defaultTo(0);
			table.integer('suggestedDay').unsigned().notNullable().defaultTo(0);
			table.boolean('noFish').notNullable().defaultTo(false);
			table.float('kcal').notNullable().defaultTo(0);
			table.float('protein').notNullable().defaultTo(0);
			table.float('carb').notNullable().defaultTo(0);
			table.float('fat').notNullable().defaultTo(0);
			table.float('sodium').notNullable().defaultTo(0);
			table.float('vitaminA').notNullable().defaultTo(0);
			table.float('vitaminC').notNullable().defaultTo(0);
			table.float('calcium').notNullable().defaultTo(0);
			table.float('dietFiber').notNullable().defaultTo(0);
			table.float('sugar').notNullable().defaultTo(0);
		})
	]);
};

exports.down = function (knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('meal')
	]);
};

exports.up = function (knex, Promise) {
	return Promise.all([
		knex.schema.createTable('mealPlanInfo', (table) => {
			table.increments('id').primary();
			table.string('name', 255).notNullable().defaultTo('');
			table.text('description').notNullable().defaultTo('');
		})
	]);
};

exports.down = function (knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('mealPlanInfo')
	]);
};
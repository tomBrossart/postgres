exports.up = function (knex, Promise) {
	return Promise.all([
		knex.schema.createTable('address', (table) => {
			table.increments('id').primary();
			table.integer('userId').unsigned().notNullable().references('user.id').onUpdate('CASCADE').onDelete('CASCADE');
			table.string('address1', 100).notNullable().defaultTo('');
			table.string('address2', 100).nullable();
			table.string('city', 50).notNullable().defaultTo('');
			table.string('state', 2).notNullable().defaultTo('');
			table.string('zip', 15).notNullable().defaultTo('');
			table.boolean('default').notNullable().defaultTo(false);
			table.boolean('billing').notNullable().defaultTo(false);
			table.text('deliveryInstructions').nullable();
		})
	]);
};

exports.down = function (knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('address')
	]);
};

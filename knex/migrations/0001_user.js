exports.up = function (knex, Promise) {
	return Promise.all([
		knex.schema.createTable('user', (table) => {
			table.increments('id').primary();
			table.string('email', 100).unique().notNullable().defaultTo('');
			table.string('password', 100).notNullable().defaultTo('');
			table.string('roles', 255).notNullable().defaultTo('["user"]');
			table.string('firstName', 50).notNullable().defaultTo('');
			table.string('lastName', 50).notNullable().defaultTo('');
			table.integer('height').nullable();
			table.integer('weight').nullable();
			table.integer('targetWeight').nullable();
			table.string('gender', 15).nullable();
			table.timestamp('birthdate').nullable();
			table.string('activityLevel', 15).nullable();
			table.integer('workActivity').nullable();
			table.text('goals').nullable();
			table.boolean('noFish').notNullable().defaultTo(false);
			table.string('phone', 50).nullable();
			table.string('imgURL', 255).nullable();
			table.text('notifications').nullable();
			table.string('notificationsEmail', 100).nullable();
			table.text('conditions').nullable();
			table.integer('previousWeight').nullable();
			table.string('stripeId', 100).nullable();
			table.string('customClaims', 1024).notNullable().defaultTo('{}');
			table.boolean('confirmed').notNullable().defaultTo(false);
			table.string('confirmCode', 255).nullable();
			table.timestamps(true, true);
		})
	]);
};

exports.down = function (knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('user')
	]);
};

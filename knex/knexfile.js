module.exports = {
    client: 'mysql',
    connection: {
        host: 'mysql',
        user: 'root',
        password: 'secret',
        database: 'tomtest'
    },
    migrations: {
        directory: './migrations'
    },
    seeds: {
        directory: './seeds'
    },
    pool: {
        min: 0
    }
};
// const db_configuration = {
// 	client: process.env.DB_CLIENT,
// 	connection: {
// 		host: process.env.HOST_NAME,
// 		user: process.env.DB_USER,
// 		password: process.env.DB_PASSWORD,
// 		database: process.env.DATABASE_NAME
// 	},
// 	pool: {
// 		min: 0
// 	}
// };


const db_configuration = {
	client: 'mysql',
	connection: {
		host: 'mysql',
		user: 'root',
		password: 'secret',
		database: 'tomtest'
	},
	pool: {
		min: 0
	}
}

exports.knex = require('knex')(db_configuration);
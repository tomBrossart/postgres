const expect = require('chai').expect;
const addTwoNumbers = require('./addTwoNumbers');
// const pingDb = require('../pingDb');
const raw = require('../raw_queries');
const mealService = require('../MealService/meals.ts');


describe('addTwoNumbers()', function () {
    it('should add two numbers', function () {

        // 1. ARRANGE
        var x = 5;
        var y = 1;
        var sum1 = x + y;

        // 2. ACT
        var sum2 = addTwoNumbers(x, y);

        // 3. ASSERT
        expect(sum2).to.be.equal(sum1);

    });
});


describe('get_meal_plans()', function() {
   it('should get meal plans', function(done) {
      // 1. ARRANGE
       let event = {
           foo: 'bar'
       };

      // 2. ACT
       raw.get_meal_plans(event, (err, result) => {
           try{
               expect(result.statusCode).to.be.equal(200);
               expect(JSON.parse(result.body).mealPlans.length).to.be.equal(9)
               done();
           } catch(error) {
               done(error)
           }
       });

   });
});

describe('get_meals()', function() {
    it('should get meals', function(done) {
        // 1. ARRANGE
        let event = {
            body: '{"columns":["id","name","noFish","suggestedDay","mealTime","imageUrl"]}',

        };
        // JSON.stringify(event);
        let context = {
          test: "test"
        };

        // 2. ACT
        mealService.get_meals(event, context, (err, result) => {
            try{
                expect(result.statusCode).to.be.equal(200);
                done();
            } catch(error) {
                done(error)
            }
        });

    });
});










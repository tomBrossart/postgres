service: MealService
path: '/meals'

package:
  include:
    - ../authorize.js
    - ../database.js
    - ../order_helper.js
    - ../response_helper.js
    - ../log_helper.js
    - ../user_helper.js
    - ../warmer.js

provider:
  name: aws
  runtime: nodejs8.10
  role: ${file(../serverless.env.yml):${self:provider.stage}.ROLE}
  region: ${file(../serverless.env.yml):${self:provider.stage}.REGION}
  timeout: 30
  stage: ${opt:stage, 'dev'}
  versionFunctions: ${file(../serverless.env.yml):${self:provider.stage}.SLS_VERSIONING}
  vpc: ${file(../serverless.env.yml):${self:provider.stage}.VPC}
  environment:
    STAGE: ${opt:stage, 'dev'}
    HOST_NAME: ${file(../serverless.env.yml):${self:provider.stage}.HOST_NAME}
    DB_USER: ${file(../serverless.env.yml):${self:provider.stage}.DB_USER}
    DB_PASSWORD:  ${file(../serverless.env.yml):${self:provider.stage}.DB_PASSWORD}
    DATABASE_NAME: ${file(../serverless.env.yml):${self:provider.stage}.DATABASE_NAME}
    DB_CLIENT: ${file(../serverless.env.yml):DB_CLIENT}
    BASE_URL: ${file(../serverless.env.yml):${self:provider.stage}.BASE_URL}
    STRIPE_KEY: ${file(../serverless.env.yml):${self:provider.stage}.STRIPE_KEY}
    CORS_ORIGIN: ${file(../serverless.env.yml):${self:provider.stage}.CORS_ORIGIN}
    S3_PUBLIC_ASSETS: ${file(../serverless.env.yml):${self:provider.stage}.S3_PUBLIC_ASSETS}
    AWS_KEY_ID: ${file(../serverless.env.yml):${self:provider.stage}.AWS_KEY_ID}
    AWS_KEY_SECRET: ${file(../serverless.env.yml):${self:provider.stage}.AWS_KEY_SECRET}
    SPARKPOST_API_KEY: ${file(../serverless.env.yml):SPARKPOST_API_KEY}

plugins:
  - serverless-plugin-warmup
  - serverless-prune-plugin

custom:
  warmup:
    schedule: 'cron(0/10 12-23 * * ? *)'
    timeout: 30
  prune:
    automatic: true
    number: 5

functions:
  get_meals:
    warmup: production
    handler: meals.get_meals
    events:
      - http:
          path: /
          method: POST
          cors: true

  get_meal:
    warmup: production
    handler: meals.get_meal_by_id
    events:
      - http:
          path: /{id}
          method: GET
          cors: true
          request:
            parameters:
              paths:
                id: true

  set_meal:
    warmup: production
    handler: meals.set_meal
    events:
      - http:
          path: /{id}
          method: POST
          cors: true
          request:
            parameters:
              paths:
                id: true

  get_meal_plan_infos:
    warmup: production
    handler: meals.get_meal_plan_infos
    events:
      - http:
          path: /plan-infos
          method: GET
          cors: true

  set_meal_plan_info:
    warmup: production
    handler: meals.set_meal_plan_info
    events:
      - http:
          path: /plan-infos/{id}
          method: POST
          cors: true
          request:
            parameters:
              paths:
                id: true

  get_meal_plans:
    warmup: production
    handler: meals.get_meal_plans
    events:
      - http:
          path: /plans
          method: GET
          cors: true


  set_meal_plan:
    warmup: production
    handler: meals.set_meal_plan
    events:
      - http:
          path: /plans/{id}
          method: POST
          cors: true
          request:
            parameters:
              paths:
                id: true

  get_plans_meals:
    warmup: production
    handler: meals.get_plans_meals
    events:
      - http:
          path: /plans/meals
          method: GET
          cors: true

  get_plan_meals_for_week:
    warmup: production
    handler: meals.get_plan_meals_for_week
    events:
      - http:
          path: /plans/{id}/{year}/{week}
          method: GET
          cors: true
          request:
            parameters:
              paths:
                id: true
                year: true
                week: true

  delete_orphan_meals:
    handler: meals.delete_orphan_meals
    events:
      - http:
          path: /clean
          method: GET
          cors: true

  get_delivery_offsets:
    handler: meals.get_delivery_offsets
    events:
    - http:
        path: /offsets
        method: GET
        cors: true

  save_delivery_offsets:
     warmup: production
     handler: meals.save_delivery_offsets
     events:
        -  http:
              path: /offsets
              method: POST
              cors: true
              request:
                 parameters:
                    paths:
                       id: true

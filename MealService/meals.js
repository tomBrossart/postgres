"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const knex = require('./database').knex;
const authorize = require('./authorize');
const RH = require('./response_helper.js');
const OH = require('./order_helper.js');
const warmer = require('./warmer');
const moment = require('moment');
exports.get_meals = (event, context, callback) => __awaiter(this, void 0, void 0, function* () {
    const ready = yield warmer.check(event, context, callback).catch(error => {
        console.log(error);
        return false;
    });
    if (!ready) {
        return;
    }
    const query_data = JSON.parse(event.body);
    const meals = yield knex('meal').column(query_data.columns).catch(error => {
        console.log(error);
        return null;
    });
    if (!meals) {
        callback(null, RH.e_response(500));
        return;
    }
    callback(null, RH.object_response(200, { meals: meals }));
});
exports.get_meal_by_id = (event, context, callback) => __awaiter(this, void 0, void 0, function* () {
    const ready = yield warmer.check(event, context, callback).catch(error => {
        console.log(error);
        return false;
    });
    if (!ready) {
        return;
    }
    const mealId = event.pathParameters.id;
    const meal = yield knex('meal').where('id', mealId).first().catch(error => {
        console.log(error);
        return null;
    });
    if (!meal) {
        callback(null, RH.e_response(500));
        return;
    }
    callback(null, RH.object_response(200, { meal: meal }));
});
exports.get_meal_plans = (event, context, callback) => __awaiter(this, void 0, void 0, function* () {
    const ready = yield warmer.check(event, context, callback).catch(error => {
        console.log(error);
        return false;
    });
    if (!ready) {
        return;
    }
    const mealPlans = yield knex('mealPlan').where('active', 1)
        .join('mealPlanInfo', 'mealPlan.mealPlanInfoId', '=', 'mealPlanInfo.id')
        .select('mealPlan.id', 'mealPlanInfo.name', 'mealPlan.name as longName', 'mealPlan.mealPlanInfoId', 'mealPlan.mealCount', 'mealPlan.cost', 'mealPlan.pickupDay', 'mealPlanInfo.description', 'mealPlanInfo.nameShort')
        .catch(error => {
        console.log(error);
        return null;
    });
    if (!mealPlans) {
        callback(null, RH.e_response(500));
        return;
    }
    callback(null, RH.object_response(200, { mealPlans: mealPlans }));
});
exports.set_meal_plan = (event, context, callback) => __awaiter(this, void 0, void 0, function* () {
    const ready = yield warmer.check(event, context, callback).catch(error => {
        console.log(error);
        return false;
    });
    if (!ready) {
        return;
    }
    const token = authorize.get_header_token(event);
    const { scopes } = yield authorize.check_privileges_claims(['superAdmin'], token);
    if (!scopes.includes('superAdmin')) {
        callback(null, RH.e_response(403));
        return;
    }
    // const mealPlanId = event.pathParameters.id
    const mealPlan = JSON.parse(event.body);
    const result = yield knex('mealPlan').where('id', mealPlan.id).update(mealPlan).catch(error => {
        console.log(error);
        return null;
    });
    if (!result) {
        callback(null, RH.e_response(500));
        return;
    }
    callback(null, RH.object_response(200, { mealPlan: mealPlan }));
});
exports.set_meal_plan_info = (event, context, callback) => __awaiter(this, void 0, void 0, function* () {
    const ready = yield warmer.check(event, context, callback).catch(error => {
        console.log(error);
        return false;
    });
    if (!ready) {
        return;
    }
    const token = authorize.get_header_token(event);
    const { scopes } = yield authorize.check_privileges_claims(['superAdmin'], token);
    if (!scopes.includes('superAdmin')) {
        callback(null, RH.e_response(403));
        return;
    }
    // const mealPlanInfoId = event.pathParameters.id
    const mealPlanInfo = JSON.parse(event.body);
    const result = yield knex('mealPlanInfo').where('id', mealPlanInfo.id).update(mealPlanInfo).catch(error => {
        console.log(error);
        return null;
    });
    if (!result) {
        callback(null, RH.e_response(500));
        return;
    }
    callback(null, RH.object_response(200, { mealPlanInfo: mealPlanInfo }));
});
exports.get_meal_plan_infos = (event, context, callback) => __awaiter(this, void 0, void 0, function* () {
    const ready = yield warmer.check(event, context, callback).catch(error => {
        console.log(error);
        return false;
    });
    if (!ready) {
        return;
    }
    const mealPlanInfos = yield knex('mealPlanInfo').catch(error => {
        console.log(error);
        return null;
    });
    if (!mealPlanInfos) {
        callback(null, RH.e_response(500));
        return;
    }
    callback(null, RH.object_response(200, { mealPlanInfos: mealPlanInfos }));
});
exports.set_meal = (event, context, callback) => __awaiter(this, void 0, void 0, function* () {
    const ready = yield warmer.check(event, context, callback).catch(error => {
        console.log(error);
        return false;
    });
    if (!ready) {
        return;
    }
    const token = authorize.get_header_token(event);
    const { scopes } = yield authorize.check_privileges_claims(['superAdmin'], token);
    if (!scopes.includes('superAdmin')) {
        callback(null, RH.e_response(403));
        return;
    }
    const meal = JSON.parse(event.body);
    const result = yield knex('meal').where('id', meal.id).update(meal).catch(error => {
        console.log(error);
        return null;
    });
    if (!result) {
        callback(null, RH.e_response(500));
        return;
    }
    callback(null, RH.object_response(200, { meal: meal }));
});
exports.get_plans_meals = (event, context, callback) => __awaiter(this, void 0, void 0, function* () {
    const ready = yield warmer.check(event, context, callback).catch(error => {
        console.log(error);
        return false;
    });
    if (!ready) {
        return;
    }
    const plansMeals = yield knex('mealPlans_meals').catch(error => {
        console.log(error);
        return null;
    });
    if (!plansMeals) {
        callback(null, RH.e_response(500));
        return;
    }
    callback(null, RH.object_response(200, { plansMeals: plansMeals }));
});
exports.get_plan_meals_for_week = (event, context, callback) => __awaiter(this, void 0, void 0, function* () {
    const ready = yield warmer.check(event, context, callback).catch(error => {
        console.log(error);
        return false;
    });
    if (!ready) {
        return;
    }
    const planId = parseInt(event.pathParameters.id);
    let year = parseInt(event.pathParameters.year);
    let week = parseInt(event.pathParameters.week);
    let meals = [];
    // Look for meal override for this specific "Week" of year (week 52 = Christmas week)
    // CAVEAT: Weeks 1-5 of the year cannot have overrides.
    if (week > 5) {
        meals = yield knex('mealPlans_meals')
            .join('meal', 'mealId', 'meal.id')
            .where('mealPlanId', planId)
            .where('week', week)
            .catch(error => {
            console.log(error);
            return null;
        });
        if (meals == null) {
            callback(null, RH.e_response(500));
            return;
        }
        if (meals.length > 0) {
            callback(null, RH.object_response(200, { meals: meals }));
            return;
        }
    }
    // No override.
    let weekDate = moment('2018-06-06');
    weekDate.year(year);
    weekDate.week(week);
    week = OH.get_week_mod(weekDate.toDate());
    meals = yield knex('mealPlans_meals')
        .join('meal', 'mealId', 'meal.id')
        .where('mealPlanId', planId)
        .where('week', week)
        .catch(error => {
        console.log(error);
        return null;
    });
    if (meals == null) {
        callback(null, RH.e_response(500));
        return;
    }
    callback(null, RH.object_response(200, { meals: meals }));
});
exports.get_delivery_offsets = (event, context, callback) => __awaiter(this, void 0, void 0, function* () {
    const ready = yield warmer.check(event, context, callback).catch(error => {
        console.log(error);
        return false;
    });
    if (!ready) {
        return;
    }
    let offsets = yield knex('settings').where('key', 'deliveryOffsets').first().catch(error => {
        console.log(error);
        return null;
    });
    if (!offsets) {
        callback(null, RH.e_response(500));
        return;
    }
    callback(null, RH.object_response(200, JSON.parse(offsets.value)));
});
exports.save_delivery_offsets = (event, context, callback) => __awaiter(this, void 0, void 0, function* () {
    const ready = yield warmer.check(event, context, callback).catch(error => {
        console.log(error);
        return false;
    });
    if (!ready) {
        return;
    }
    const token = authorize.get_header_token(event);
    const { scopes } = yield authorize.check_privileges_claims(['superAdmin'], token);
    if (!scopes.includes('superAdmin')) {
        callback(null, RH.e_response(403));
        return;
    }
    const update = { value: event.body };
    const res = yield knex('settings').where('key', 'deliveryOffsets').update(update)
        .catch(error => {
        console.log(error);
        return null;
    });
    if (!res) {
        callback(null, RH.e_response(500));
        return;
    }
    callback(null, RH.object_response(200, { success: true }));
});
exports.delete_orphan_meals = (event, context, callback) => __awaiter(this, void 0, void 0, function* () {
    const ready = yield warmer.check(event, context, callback).catch(error => {
        console.log(error);
        return false;
    });
    if (!ready) {
        return;
    }
    const token = authorize.get_header_token(event);
    const { scopes } = yield authorize.check_privileges_claims(['superAdmin'], token);
    if (!scopes.includes('superAdmin')) {
        callback(null, RH.e_response(403));
        return;
    }
    let rows = yield knex('meal').whereNotExists(function () {
        this.select('*').from('mealPlans_meals').whereRaw('meal.id = mealPlans_meals.mealId');
    }).del().catch(error => {
        console.log(error);
        return null;
    });
    if (!rows) {
        callback(null, RH.e_response(500));
        return;
    }
    callback(null, RH.object_response(200, {
        success: true,
        deleted: rows
    }));
});

const RH = require('./response_helper.js');
const warmer = require('./testwarmer.js');

const db_configuration = {
    client: 'mysql',
    connection: {
        host: 'mysql',
        user: 'root',
        password: 'secret',
        database: 'tomtest'
    },
    pool: {
        min: 0
    }
}
const knex = require('knex')(db_configuration);


exports.get_meal = (event, callback) => {
    const id = parseInt(event.pathParameters.id);
    knex('meal')
        .where('id', id)
        .limit(1)
        .then(meals => {
            if (meals.length > 0) {
                callback(null, RH.object_response(200, {meal: meals[0]}));
            } else {
                callback(null, RH.e_response(400, 'Invalid Id'));
            }
        })
        .catch(error => {
            callback(null, RH.e_response(500));
        });
};

exports.get_meal_plans = (event, callback) => {
    knex('mealPlan')
        .where('active', 1)
        // .join('mealPlanInfo', 'mealPlan.mealPlanInfoId', '=', 'mealPlanInfo.id')
        .then(mealPlans => {
            callback(null, RH.object_response(200, {mealPlans: mealPlans}));
        })
        .catch(error => {
            callback(null, RH.e_response(500));
        });
};
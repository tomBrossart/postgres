const DefaultMessages = {
	400: 'Bad Request',
	401: 'Not Authorized',
	403: 'Forbidden',
	404: 'Not Found',
	500: 'Server Error'
};

exports.e_response = (statusCode, message) => {
	if (!message) {
		message = DefaultMessages[statusCode];
	}
	let response = {
		statusCode: statusCode,
		headers: {
			'Access-Control-Allow-Origin': process.env.CORS_ORIGIN
		},
		body: JSON.stringify({error: message})
	};
	console.log('RESPONSE ' + JSON.stringify(response));
	return response;
};

exports.result_response = (statusCode, message) => {
	if (!message) {
		message = DefaultMessages[statusCode];
	}
	let response = {
		statusCode: statusCode,
		headers: {
			'Access-Control-Allow-Origin': process.env.CORS_ORIGIN
		},
		body: JSON.stringify({result: message})
	};
	console.log('RESPONSE ' + JSON.stringify(response));
	return response;
};

exports.object_response = (statusCode, obj) => {
	let response = {
		statusCode: statusCode,
		headers: {
			'Access-Control-Allow-Origin': process.env.CORS_ORIGIN || "test filler"
		},
		body: JSON.stringify(obj)
	};
	//console.log('RESPONSE ' + JSON.stringify(response));
	return response;
};

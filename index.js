var mysql      = require('mysql');
var connection = mysql.createConnection({
    host     : 'mysql',
    user     : 'root',
    password : 'secret',
    insecureAuth : true
});

connection.connect(function(err) {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }

    console.log('connected as id ' + connection.threadId);
});

//
// const db_configuration = {
//     client: 'mysql',
//     connection: {
//         host: 'mysql',
//         user: 'root',
//         password: 'secret',
//         database: 'tomtest'
//     },
//     pool: {
//         min: 0
//     }
// }
//
// const knex = require('knex')(db_configuration);
//
// knex('user').then(result => {
//     console.log("result from knex users query is...", result)
// });

connection.end(function(err) {
    if (err) {
        console.error('error disconnecting: ' + err.stack);
        return;
    }});

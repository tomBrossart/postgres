#!/bin/bash

npm install
npm install -g knex
knex migrate:latest --knexfile knex/knexfile.js
knex seed:run --knexfile knex/knexfile.js
sed -i 's/\.\//\.\.\//g' MealService/meals.ts
npm test
npm start



exports.check = exports.check = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    return new Promise((resolve, reject) => {
        if (event.source === 'serverless-plugin-warmup') {
            callback(null, 'Lambda is warm!');
            reject('Warming Lambda');
        } else {
            resolve(true);
        }
    });
};